from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    return {"picture_url": content["photos"][0]["src"]["original"]}

def get_weather_data(city, state):
    params = {
        "q": f"{city},{state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()
    #need to get weather from long, lat
    #if empty list, we need to use try, except block
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()

    return {
        #find the path based on insomnia
        #content is giant dictionary
        #only way to look at any item in a list is to use an index, so have to use [0]
        #and once we have the 0 index, we use the key to get the value
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"]
    }
