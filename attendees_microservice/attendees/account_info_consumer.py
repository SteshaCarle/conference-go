from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


def update_or_delete_account(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(**content)
    else:
        AccountVO.objects.filter(email=email).delete()

while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        #declare a fanout exchange named "account_info"
        channel.exchange_declare(exchange="account_info", exchange_type="fanout")
        #declare a randomly-named queue
        result = channel.queue_declare(queue='', exclusive=True)
        #get the queue name of the randomly-named queue
        queue_name = result.method.queue
        #bind the queue to the "account_info" exchange
        channel.queue_bind(exchange='account_info', queue=queue_name)
        #do a basic_consume for the queue name that calls
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_or_delete_account,
            #on_message_callback=update_or_delete_account,
            auto_ack=True,
        )
        #    function above
        #tell the channel to start consuming
        channel.start_consuming()

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
